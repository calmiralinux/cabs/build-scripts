# Build Instructions

Сборочные инструкции для полной редакции Calmira GNU/Linux-libre (без `*.txz`
пакетов; система полностью).

## Build order

| № | Mode          | User                                 | Status   | Description                         |
|---|---------------|--------------------------------------|----------|-------------------------------------|
| 1 | `prepare`     | `root` (host system)                 | Complete | Preparing to build the system       |
| 2 | `tools`       | `calm` (host system)                 | Complete | Building a cross compiler           |
| 3 | `pre_chroot`  | `root` (host system)                 | Complete | Preparing to enter the chroot       |
| 4 | `post_chroot` | `root` (in the `chroot` environment) | Complete | Completing the cross compiler build |
| 5 | `core`        | `root` (in the `chroot` environment) | Test     | Building the base system            |
| 6 | `extended`    | `root` (in the `chroot` environment) | WIP      | Building the extended system        |
| 7 | `iso`         | `root` (host system)                 | WIP      | Generation of iso system image      |

## Dependencies

1. `bash` (`/bin/sh` должен быть символической или жёсткой ссылкой на `bash`)
2. `binutils`
3. `bison`
4. `coreutils`
5. `diffutils`
6. `findutils`
7. `gawk`
8. `gcc`
9. `grep`
10. `gzip`
11. `linux`/`linux-libre`
12. `m4`
13. `make`
14. `patch`
15. `perl-5`
16. `python-3`
17. `sed`
18. `tar`
19. `texinfo`
20. `xz`

### Other dependencies

1. `wget`
2. `git`
3. `rust` (`rustc`, `cargo`)
4. `squashfs-tools`
5. `xorriso`

## Использование

```bash
./bs -m MODE
```

## Информация о сборочных инструкциях

По возможности используются сборочные инструкции из руководств LFS и BLFS. Кроме того, используются патчи из этих руководств.

<!-- ## Build modes

| Name         | Info                                      | Instructions | Status                   |
|--------------|-------------------------------------------|--------------|--------------------------|
| `prepare`    | Подготовительные к сборке операции        | 3            | Закончено                |
| `toolchain`  | Сборка тулчейна                           | 5            | Закончено                |
| `tmp_tools`  | Сборка временного инструментария          | 17           | `ncurses` build error    |
| `pre_chroot` | Подготовка ко входу в окружение chroot    | 2            | Нуждается в тестировании |
| `tmp_tools2` | Сборка временного инструментария в chroot | 8            | Нуждается в тестировании |
| `core`       | Сборка базовой системы                    | 0            | WIP                      |
| `extra`      | Сборка Extended-редакции системы          | 0            | WIP                      | -->
