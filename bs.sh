#!/bin/bash -e
# Base functions and variables for Calmira's build scripts
# Copyright (C) 2023, Michail Krasnov <linuxoid85@gmail.com>

set +h
umask 022

LC_ALL=POSIX
CALM_TGT=$(uname -m)-calm-linux-gnu # новое объявление на всякий случай
CONFIG_SITE=$CALM/usr/share/config.site

export LC_ALL CALM_TGT CONFIG_SITE

function clean_src() {
  for f in *
  do
    if [ -d $f ]; then
      echo -e "\v\t\a\e[1;33mFound directory '\e[0m$f\e[1;33m'. Delete.\e[0m"
      rm -rvf $f
      echo -e "\v\t\a\e[1;32mRemove '\e[0m$f\e[1;32m' OK.\e[0m"
    fi
  done
}

function test_start() {
  echo -e "\a\t\v\e[1;32m--== START TESTS... ==--\e[0m\v"
  sleep 4
}

function test_end() {
  echo -e "\a\t\v\e[1;32m--== END TESTS! ==--\e[0m\v"
  sleep 3
}

export clean_src
