#!/usr/bin/python3

import os
import sys

pkg = sys.argv[1]
ver = sys.argv[2]

"""
TOML Config structure:

```toml
[settings]
dir = "core"
path = "/bin:/sbin:/usr/bin:/usr/sbin"
calm = "/mnt/calm"
calm_tgt = "x86_64-calm-linux-gnu"
makeflags = "-j4"
src_dir = "/mnt/calm/usr/src"

[build]
packages = [
    "acl",
    "attr",
    "shadow",
    "gcc"
]
```
"""

content = fr"""#!/bin/bash
# Build script for package '{pkg}-{ver}'
set -Eeuo pipefail

# Script with functions for clean source tree
. /etc/bs.sh

NAME="{pkg}"
VERSION="{ver}"

cd $SRC_DIR
tar -xf $NAME-$VERSION.tar.*
cd      $NAME-$VERSION

if [ $CHECK == "yes" ]; then
  echo -e "\a\n\n\e[1;32m==>\e[0m Checking package '$NAME'..."
  make check 2>&1 | tee /var/log/$NAME-$VERSION-check.log
  echo -e "\a\n\n\e[1;32m==>\e[0m Checking package '$NAME' complete! Log file: '/var/log/$NAME-$VERSION-check.log'"
fi

cd $SRC_DIR
clean_src
"""

with open(pkg, 'w') as f:
    f.write(content)

script = f"""hx {pkg}
chmod +x {pkg}
git add {pkg}
git commit -m "Add script for package \"{pkg}-{ver}\""
git push
"""

os.system(script)
