#!/bin/bash
# Build script for package 'kernfs-2.0'
set -Eeuo pipefail

# Script with functions for clean source tree
. /etc/bs.sh

NAME="kernfs"
VERSION="2023.0"

mkdir -pv $CALM/{dev,proc,sys,run}

mount -v --bind /dev $CALM/dev
mount -v --bind /dev/pts $CALM/dev/pts
mount -vt proc proc $CALM/proc
mount -vt sysfs sysfs $CALM/sys
mount -vt tmpfs tmpfs $CALM/run

if [ -h $CALM/dev/shm ]; then
  mkdir -pv $CALM/$(readlink $CALM/dev/shm)
else
  mount -vt tmpfs -o nosuid,nodev tmpfs $CALM/dev/shm
fi

echo -e "\n\n\a\e[1;31m==>\e[0m Copy CABS files..."
echo -e "\e[1;31m[DBG]\e[0m PWD dir: $PWD ($(pwd))"


mkdir -pv $CALM/usr/share/bs
cp -rv ./* $CALM/usr/share/bs
cp -v $CALM/usr/share/bs/bs.sh $CALM/etc/bs.sh

cat > $CALM/usr/bin/bs << "EOF"
#!/bin/bash

cd /usr/share/bs

echo "[ $(date) ] post_chroot" >> /var/log/build_mode.log
./bs -m post_chroot
sleep 2
echo -e "\n\n\n\t\a\e[1;32m'post_chroot' mode: COMPLETE\e[0m"

echo -e "\e[1mContinue building??? (y/n)\e[0m"
read run

if [ $run != "y" ]; then
	exit 1
fi

echo "[ $(date) ] core" >> /var/log/build_mode.log
./bs -m core
echo -e "\n\n\n\t\a\e[1;32m'core' mode: COMPLETE\e[0m"

EOF
chmod +x $CALM/usr/bin/bs

chroot $CALM /usr/bin/env -i \
	HOME=/root \
	TERM="$TERM" \
	PS1="(CHROOT ENV) \u:\w\$ " \
	PATH=/usr/bin:/usr/sbin \
	/bin/bash --login -c /usr/bin/bs

# cat << "EOF"
# As user root, run the following command to enter the environment that is,
# at the moment, populated with nothing but temporary tools:

# 	chroot "$CALM" /usr/bin/env -i   \
# 		HOME=/root                   \
# 		TERM="$TERM"                 \
# 		PS1='(calm chroot) \u:\w\$ ' \
# 		PATH=/usr/bin:/usr/sbin      \
# 		/bin/bash --login

# 	cd /usr/share/bs

# 	./bs -m post_chroot
# 	./bs -m core
# 	./bs -m extended # Optional
# EOF
