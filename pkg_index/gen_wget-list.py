#!/usr/bin/python3

import toml
import sys

data = toml.load(sys.argv[1])

for pkg in data['package']:
    if pkg.get("url") is None:
        continue
    with open("wget-list", "a") as f:
        f.write(f"{pkg['url']}\n")
