#!/bin/bash
# Build script for package 'openssl-3.0.8'
set -Eeuo pipefail

# Script with functions for clean source tree
. /etc/bs.sh

NAME="openssl"
VERSION="3.0.8"

cd $SRC_DIR
tar -xf $NAME-$VERSION.tar.*
cd      $NAME-$VERSION

./config --prefix=/usr \
  --openssldir=/etc/ssl \
  --libdir=lib \
  shared \
  zlib-dynamic

make

###############################################################################
# WARNING: the test '30-test_afalg.t' is known to fail on some kernel         #
# configurations (depending on inconsistent values of 'CONFIG_CRYPTO_USER_API*#
# ' settings). If it fails, it can safely be ignored.                         #
###############################################################################

if [ $CHECK == "yes" ]; then
  echo -e "\a\n\n\e[1;32m==>\e[0m Checking package '$NAME'..."
  make test 2>&1 | tee /var/log/$NAME-$VERSION-check.log
  echo -e "\a\n\n\e[1;32m==>\e[0m Checking package '$NAME' complete! Log file: '/var/log/$NAME-$VERSION-check.log'"
fi

sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
make MANSUFFIX=ssl install

mv -v /usr/share/doc/openssl /usr/share/doc/$NAME-$VERSION
cp -vfr doc/* /usr/share/doc/$NAME-$VERSION

cd $SRC_DIR
clean_src
