#!/bin/bash
# Build script for package 'e2fsprogs-1.47.0'
set -Eeuo pipefail

# Script with functions for clean source tree
. /etc/bs.sh

NAME="e2fsprogs"
VERSION="1.47.0"

cd $SRC_DIR
tar -xf $NAME-$VERSION.tar.*
cd      $NAME-$VERSION

mkdir -v build
cd build

../configure --prefix=/usr \
  --sysconfdir=/etc \
  --enable-elf-shlibs \
  --disable-libblkid \
  --disable-libuuid \
  --disable-uuidd \
  --disable-fsck

make

###############################################################################
# WARNING: the test 'u_direct_io' is known to fail on some systems.           #
###############################################################################

if [ $CHECK == "yes" ]; then
  echo -e "\a\n\n\e[1;32m==>\e[0m Checking package '$NAME'..."
  make check 2>&1 | tee /var/log/$NAME-$VERSION-check.log
  echo -e "\a\n\n\e[1;32m==>\e[0m Checking package '$NAME' complete! Log file: '/var/log/$NAME-$VERSION-check.log'"
fi

make install

rm -fv /usr/lib/{libcom_err,libe2p,libext2fs,libss}.a

gunzip -v /usr/share/info/libext2fs.info.gz
install-info --dir-file=/usr/share/info/dir /usr/share/info/libext2fs.info

makeinfo -o      doc/com_err.info ../lib/et/com_err.texinfo
install -v -m644 doc/com_err.info /usr/share/info
install-info --dir-file=/usr/share/info/dir /usr/share/info/com_err.info

# NOTE: /etc/mke2fs.conf contains the default value of various command line options
# of mke2fs. You may edit the file to make the default values suitable for your need.
# For example, some utilities (not in Calmira GNU/Linux-libre) cannot recognize a
# ext4 file system with metadata_csum_seed feature enabled. If you need such an
# utility, you may remove the feature from the default ext4 feature list with the
# command:

##sed 's/metadata_csum_seed,//' -i /etc/mke2fs.conf

# See 'mke2fs.conf (5)' man page for details.

cd $SRC_DIR
clean_src
